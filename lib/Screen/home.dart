import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pqrsproapp/Screen/registerpqrs.dart';
import 'package:pqrsproapp/models/token_model.dart';

class Home extends StatefulWidget {
  // ignore: non_constant_identifier_names
  Home({Key key, this.title, this.token}) : super(key: key);
  final String title;
  final String token;

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<Home> {
  List<Map<String, dynamic>> datosUser = [
  ];

  Future<void> _showMyDialog() async {
    GlobalKey _formKey = GlobalKey<FormState>();

    TextEditingController name = TextEditingController();
    TextEditingController id = TextEditingController();
    TextEditingController rol = TextEditingController();

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Nuevo usuario'),
          content: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  TextFormField(
                    controller: name,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'ingrese valor';
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Nombre de usuario'),
                  ),
                  TextFormField(
                    controller: id,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'ingrese valor';
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Id'),
                  ),
                  TextFormField(
                    controller: rol,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'ingrese valor';
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Tipo de usuario'),
                  )
                ],
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Salir'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Guardar'),
              onPressed: () {
                setState(() {
                  datosUser.add({
                    'name': name.text,
                    'id': id.text,
                    'type_user': rol.text
                  });
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showMyDialogEdit(
      {String nameData, String idData, String rolData, int index}) async {
    GlobalKey _formKey = GlobalKey<FormState>();

    TextEditingController name = TextEditingController(text: nameData);
    TextEditingController id = TextEditingController(text: idData);
    TextEditingController rol = TextEditingController(text: rolData);

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Nuevo usuario'),
          content: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  TextFormField(
                    controller: name,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'ingrese valor';
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Nombre de usuario'),
                  ),
                  TextFormField(
                    controller: id,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'ingrese valor';
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Id'),
                  ),
                  TextFormField(
                    controller: rol,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'ingrese valor';
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Tipo de usuario'),
                  )
                ],
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Salir'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Editar'),
              onPressed: () {
                setState(() {
                  datosUser[index] = {
                    'name': name.text,
                    'id': id.text,
                    'type_user': rol.text
                  };
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bienvenido '),

        // actions: <Widget>[
        //   IconButton(
        //     icon: const Icon(Icons.add),
        //     tooltip: 'Crear PQRS',
        //     onPressed: () {
        //       Navigator.of(context).push(
        //         MaterialPageRoute(
        //           builder: (BuildContext context) => RegisterPqrs(
        //             token: this.widget.token,
        //           ),
        //         ),
        //       );
        //     },
        //   ),
        // ],
      ),
      body: Container(
        constraints: BoxConstraints.expand(),
        child: DataTable(
          columnSpacing: 0,
          columns: <DataColumn>[
            DataColumn(
              label: Text(
                'Nombre',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                'ID',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                'Rol',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                'Editar',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                'Borrar',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
          ],
          rows: datosUser
              .map<DataRow>((e) => DataRow(cells: <DataCell>[
                    DataCell(Text(e['name'])),
                    DataCell(Text(e['id'])),
                    DataCell(Text(e['type_user'])),
                    DataCell(IconButton(
                      icon: Icon(
                        Icons.edit,
                        color: Colors.green,
                      ),
                      onPressed: () {
                        _showMyDialogEdit(
                            idData: e['id'],
                            index: datosUser.indexOf(e),
                            nameData: e['name'],
                            rolData: e['type_user']);
                      },
                    )),
                    DataCell(IconButton(
                      icon: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        setState(() {
                          datosUser.removeAt(datosUser.indexOf(e));
                        });
                      },
                    )),
                  ]))
              .toList(),
        ),
      ),
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: _showMyDialog,
        child: Icon(Icons.add),
      ),
    );
  }
}
