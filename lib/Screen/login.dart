import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pqrsproapp/Bloc/user_bloc.dart';
import 'package:pqrsproapp/Screen/restablecercontrase%C3%B1a.dart';
import 'package:pqrsproapp/models/usuario_model.dart';

import 'Registre.dart';
import 'home.dart';

class Login extends StatefulWidget {
  Login({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyLoginState createState() => _MyLoginState();
}

class _MyLoginState extends State<Login> {
  UserBloc userBloc;
  Usuario user = new Usuario();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    userBloc = UserBloc(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Image.asset(
              'assets/img/pqrs.png',
              height: 180,
            ),
          ),
          Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.mail), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                            decoration: InputDecoration(hintText: 'Email'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              user.username = value;
                              return null;
                            },
                            onSaved: (String value) {
                              if (value.isNotEmpty) {
                                user.username = value;
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.lock), onPressed: null),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 20, left: 10),
                          child: TextFormField(
                            decoration: InputDecoration(hintText: 'Password'),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return null;
                            },
                            onSaved: (String value) {
                              if (value.isNotEmpty) {
                                user.password = value;
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Container(
                      child: RaisedButton(
                        onPressed: _submitLogin,
                        child: Text('Submit'),
                        color: Colors.teal,
                        clipBehavior: Clip.hardEdge,
                        elevation: 10,
                        disabledColor: Colors.blueGrey,
                        disabledElevation: 10,
                        disabledTextColor: Colors.white,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => RegisterUser())),
                  child: Container(
                    child: Text('Registrar',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => RestablecerContrasenaUser())),
                  child: Container(
                    child: Text('Restablecer contraseña',
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue)),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error Login'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Error ingrese todos los datos'),
                Text('Vuelva a ingresar los datos'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _submitLogin() async {

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => Home(
          title: 'Bienvenido',
          token: 'value.object',
        ),
      ),
    );

    final FormState formState = _formKey.currentState;
    if (!formState.validate()) {
      _showMyDialog();
    } else {
      //_userBloc = UserBloc(context);
      formState.save();
      print(user.username);
      print(user.password);
      userBloc.loginUser(user.username, user.password).then((value) {
        print(value.object);
        if (value.statusResponse != 200) {
          _showMyDialog();
        } else {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => Home(
                title: 'Bienvenido',
                token: value.object,
              ),
            ),
          );
        }
      });
    }
  }
}
