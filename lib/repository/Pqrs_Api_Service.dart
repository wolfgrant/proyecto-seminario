import 'dart:convert';
import 'dart:io';

import 'package:pqrsproapp/Constants/constants.dart';
import 'package:http/http.dart' as http;
import 'package:pqrsproapp/models/apiresponse_model.dart';
import 'package:pqrsproapp/models/error_model.dart';

import '../models/pqrs_model.dart';


class PqrsApiServices {
  Pqrs _pqrs;
  ErrorApiResponse _error;
  final _constants = Constants();

  PqrsApiServices();

  Future<ApiResponse> RegistrarPqrs(_pqrs, String token) async {
    Map data = _pqrs.ToJson();
    var body = json.encode(data);
    print(_pqrs.toString());
    ApiResponse apiResponse = ApiResponse();
    var response = await http
        .post(
            "http://ec2-52-14-8-153.us-east-2.compute.amazonaws.com:9091/Pqrs/registrar",
            headers: {
              HttpHeaders.authorizationHeader: token,
              HttpHeaders.contentTypeHeader: "application/json"
            },
            body: body)
        .then((response) {
      var resBody = json.decode(response.body)['payload'];
      apiResponse.statusResponse = response.statusCode;
      if (apiResponse.statusResponse == 200) {
        apiResponse.object = resBody;
        apiResponse.message = 'succes!';
        print(apiResponse.object);
      } else {
        _error = ErrorApiResponse.fromJson(resBody);
        apiResponse.object = _error;
        apiResponse.message = 'failure!';
      }
      return apiResponse;
    });

    return response;
  }

  Future<bool> isSignedIn() async {
    return true;
  }
}
