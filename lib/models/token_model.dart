class TokenpiResponse {
  String token;
  String username;
  int id;

  TokenpiResponse({token,username,id});

  factory TokenpiResponse.fromJson(Map<String, dynamic> ParsedJson) {
    return TokenpiResponse(
      token: ParsedJson["token"],
      username: ParsedJson["username"],
      id: ParsedJson["id"],
    );
  }

  Map<String, dynamic> ToJson() => {
    "token": token,
    "username": username,
    "id": id,
  };

}
